"use strict";

window.onload = function () {
    var htmlBoard = document.getElementById("board");
    Str8ts.board = new Board(htmlBoard);
    Str8ts.board.initFromQueryString(window.location.search);
    Str8ts.solver = new Solver(Str8ts.board);
    Str8ts.ui = new Str8tsUI(Str8ts.board, Str8ts.solver);

    htmlBoard.addEventListener("click", function (ev) { Str8ts.ui.clickHandler(ev) }, false);
    htmlBoard.addEventListener("mousedown", function (ev) { if (ev.ctrlKey) ev.preventDefault(); }, false); // stop TD highlighting
    window.addEventListener("keypress", function (ev) { Str8ts.ui.keyPressHandler(ev) }, false);
    window.addEventListener("keydown", function (ev) { Str8ts.ui.keyDownHandler(ev) }, false);
    document.getElementById("highlight").addEventListener("click", function (ev) { Str8ts.ui.highlightHandler(ev) }, false);
    document.getElementById("setti").addEventListener("click", function (ev) { Str8ts.ui.settiHandler(ev) }, false);
    document.getElementById("colors").addEventListener("click", function (ev) { Str8ts.ui.colorsHandler(ev) }, false);
    document.getElementById("bcolumns").addEventListener("click", function (ev) { Str8ts.ui.manualSettiColumnHandler(ev) }, false);
    document.getElementById("tournament").addEventListener("click", function (ev) { Str8ts.ui.toggleChallengeHandler(ev) }, false);
    document.getElementById("tournament").checked = false;  // because of reloading
    document.getElementById("eingabe").addEventListener("submit", function (ev) { Str8ts.ui.urlFormHandler(ev) }, false);
    Str8ts.startTime = Date.now();
    setInterval(Str8ts.updateTimer, 1000);
};

var Str8ts = {
    FLAG_NO_DIGIT: 0,
    FLAG_DIGIT: 1,
    FLAG_DIGIT_TO_REMOVE: 2,
    FLAG_NEW_SINGLE: 4,

    maxStrategySteps: 99,

    history: [],

    loopXY: function (f) {
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                f(y, x);
            }
        }
    },

    setNewBoard: function(board) {
        Str8ts.board = board;
        Str8ts.solver.board = board;
        Str8ts.ui.board = board;
    },

    updateTimer: function () {
        var t = document.getElementById("time").firstChild;
        var ms = Date.now() - Str8ts.startTime;
        var min = ("00" + Math.floor(ms / 1000 / 60)).substr(-2);
        var sec = ("00" + (Math.round(ms / 1000) % 60)).substr(-2);
        t.nodeValue = min + ":" + sec;
    },

    HCompartment: (function () {
        var fn = function (row, start, end) {
            this.row = row;
            this.start = start;
            this.end = end;
            this.len = end + 1 - start;
        };
        fn.prototype = {
            get: function (index) {
                return Str8ts.board.board[this.row][index]
            },
            updateCell: function (index) {
                Str8ts.board.updateCell(this.row, index)
            },
            setNum: function (index, num, val) {
                if (!Str8ts.board.lockedcell[this.row][index]) {
                    Str8ts.board.board[this.row][index][num] = val;
                }
            },
            delNum: function (index, num, markOnly) {
                var curNum = Str8ts.board.board[this.row][index][num];
                if (curNum && !Str8ts.board.lockedcell[this.row][index]) {
                    Str8ts.board.board[this.row][index][num] = markOnly ? Str8ts.FLAG_DIGIT_TO_REMOVE : Str8ts.FLAG_NO_DIGIT;
                    return curNum;
                } else {
                    return 0;
                }
            },
            isWhite: function (index) {
                return Str8ts.board.boardcolor[this.row][index];
            },
            isLocked: function (index) {
                return Str8ts.board.lockedcell[this.row][index];
            },
            isSolved: function (index) {
                return Str8ts.board.solved[this.row][index];
            }
        };
        return fn;
    }()),

    VCompartment: (function () {
        var fn = function (column, start, end) {
            this.col = column;
            this.start = start;
            this.end = end;
            this.len = end + 1 - start;
        };
        fn.prototype = {
            get: function (index) {
                return Str8ts.board.board[index][this.col]
            },
            updateCell: function (index) {
                Str8ts.board.updateCell(index, this.col)
            },
            setNum: function (index, num, val) {
                if (!Str8ts.board.lockedcell[index][this.col]) {
                    Str8ts.board.board[index][this.col][num] = val;
                }
            },
            delNum: function (index, num, markOnly) {
                var curNum = Str8ts.board.board[index][this.col][num];
                if (curNum && !Str8ts.board.lockedcell[index][this.col]) {
                    Str8ts.board.board[index][this.col][num] = markOnly ? Str8ts.FLAG_DIGIT_TO_REMOVE : Str8ts.FLAG_NO_DIGIT;
                    return curNum;
                } else {
                    return 0;
                }
            },
            isWhite: function (index) {
                return Str8ts.board.boardcolor[index][this.col];
            },
            isLocked: function (index) {
                return Str8ts.board.lockedcell[index][this.col];
            },
            isSolved: function (index) {
                return Str8ts.board.solved[index][this.col];
            }
        };
        return fn;
    }()),

    /**
     * Additional (dynamic) information about compartments
     */
    CompartmentInfo: (function() {
        var fn = function (compartment) {
            this.compartment = compartment;
            this.update(false);
        };
        fn.prototype = {
            update: function (markOnly) {
                var cell, n, digits, min, max;
                var reachableMin = 0, reachableMax = 8, compartmentMin = 8, compartmentMax = 0;

                if (markOnly)
                    return;
                this.digitCount = [0,0,0,0,0,0,0,0,0];
                this.digitPosition = [[],[],[],[],[],[],[],[],[]];
                for (cell = this.compartment.start; cell <= this.compartment.end; cell++) {
                    digits = this.compartment.get(cell);
                    min = 8;
                    max = 0;
                    for (n = 0; n < 9; n++) {
                        if (!digits[n]) {
                            continue;
                        }
                        this.digitCount[n]++;
                        this.digitPosition[n].push(cell);
                        if (n < min) {
                            min = n;
                        }
                        if (n > max) {
                            max = n;
                        }
                    }
                    if (reachableMin < min - this.compartment.len + 1) {
                        reachableMin = min - this.compartment.len + 1;
                    }
                    if (reachableMax > max + this.compartment.len - 1) {
                        reachableMax = max + this.compartment.len - 1;
                    }
                    if (compartmentMin > min) {
                        compartmentMin = min;
                    }
                    if (compartmentMax < max) {
                        compartmentMax = max;
                    }
                }
                this.min = Math.max(reachableMin, compartmentMin);
                this.max = Math.min(reachableMax, compartmentMax);
                this.sureMin = compartmentMax - this.compartment.len + 1;
                this.sureMax = compartmentMin + this.compartment.len - 1;
            },

            getClone: function() {
                var copy = new Str8ts.CompartmentInfo(this.compartment);
                copy.min = this.min;
                copy.max = this.max;
                copy.sureMin = this.sureMin;
                copy.sureMax = this.sureMax;
                copy.digitCount = this.digitCount.slice();
                copy.digitPosition = [];
                for (var i = 0; i < 9; i++) {
                    copy.digitPosition[i] = this.digitPosition[i].slice();
                }
                return copy;
            }
        };
        return fn;
    }())
};


var Board = function (domElement) {
    if (typeof domElement == 'undefined') {
        return;
    }
    this.board = new Array(9);
    this.boardcolor = new Array(9);
    this.solved = new Array(9);
    this.lockedcell = new Array(9);
    this.domBoard = domElement;
    this.cellToHCompartmentIndex = new Array(9);
    this.cellToVCompartmentIndex = new Array(9);
    for (var y = 0; y < 9; y++) {
        this.board[y] = new Array(9);
        this.boardcolor[y] = new Array(9);
        this.solved[y] = new Array(9);
        this.lockedcell[y] = new Array(9);
        this.cellToHCompartmentIndex[y] = new Array(9);
        this.cellToVCompartmentIndex[y] = new Array(9);
        for (var x = 0; x < 9; x++) {
            this.board[y][x] = [ 1, 1, 1, 1, 1, 1, 1, 1, 1 ];
            this.boardcolor[y][x] = true;
            this.solved[y][x] = 0;
            this.lockedcell[y][x] = false;
            domElement.rows[y].cells[x + 1].innerHTML = "1 2 3<br>4 5 6<br>7 8 9";
        }
    }
    this.compartments = [];     // static info
    this.compartmentInfo = [];  // dynamic info, index in sync with this.compartments
    this.highlightedNewSingle = false;
};

Board.prototype = {
    getClone: function() {
        var copy = new Board();
        copy.board = new Array(9);
        copy.boardcolor = this.boardcolor;
        copy.solved = new Array(9);
        copy.domBoard = this.domBoard;
        copy.lockedcell = this.lockedcell;
        copy.compartments = this.compartments;
        copy.cellToHCompartmentIndex = this.cellToHCompartmentIndex;
        copy.cellToVCompartmentIndex = this.cellToVCompartmentIndex;
        copy.highlightedNewSingle = this.highlightedNewSingle;
        for (var y = 0; y < 9; y++) {
            copy.solved[y] = this.solved[y].slice();
            copy.board[y] = new Array(9);
            for (var x = 0; x < 9; x++) {
                copy.board[y][x] = this.board[y][x].slice();
            }
        }
        copy.compartmentInfo = [];
        for (var i = 0; i < this.compartmentInfo.length; i++) {
            copy.compartmentInfo.push(this.compartmentInfo[i].getClone());
        }
        return copy;
    },

    initFromQueryString: function (query) {
        var offset, num, black, zeroCharCode;
        var that = this;

        function setFieldBlack(row, col, td, num) {
            td.className = "black";
            td.innerHTML = num ? num : "";
            that.board[row][col] = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            if (num) {
                that.board[row][col][num - 1] = 1;
            }
            that.boardcolor[row][col] = false;
            that.solved[row][col] = num;
        }

        query = query.match(/\d{162,}/);
        if (query === null) {
            document.getElementById("boardarea").style.display = "none";
            document.getElementById("rightmenu").style.display = "none";
            document.getElementById("eingabe").style.display = "block";
            return false;
        } else {
            document.getElementById("boardarea").style.display = "block";
            document.getElementById("rightmenu").style.display = "block";
            document.getElementById("eingabe").style.display = "none";
            query = query[0];
        }
        offset = (query.length > 240) ? 162 : 81;
        zeroCharCode = "0".charCodeAt(0);
        Str8ts.loopXY(function (y, x) {
            num = query.charCodeAt(y * 9 + x) - zeroCharCode;
            black = query.charCodeAt(y * 9 + x + offset) - zeroCharCode;
            if (!num && !black) {
                return;
            }
            that.lockedcell[y][x] = true;
            if (black) {
                setFieldBlack(y, x, that.domBoard.rows[y].cells[x + 1], num);
            }
            else {
                that.board[y][x] = [0, 0, 0, 0, 0, 0, 0, 0, 0];
                that.board[y][x][num - 1] = 1;
                that.domBoard.rows[y].cells[x + 1].className = "locked";
                that.updateCell(y, x);
            }
        });
        this.initHorizontalCompartments();
        this.initVerticalCompartments();
        this.initCellsToCompartmentIndexes();
        return true;
    },

    initHorizontalCompartments: function () {
        var y, x, start;

        for (y = 0; y < 9; y++) {
            x = 0;
            while (x < 9) {
                while (x < 9 && !this.boardcolor[y][x]) x++; // skip black
                start = x;
                while (x < 9 && this.boardcolor[y][x]) x++;  // skip white
                if (start != 9) {
                    var compartment = new Str8ts.HCompartment(y, start, x - 1);
                    this.compartments.push(compartment);
                    this.compartmentInfo.push(new Str8ts.CompartmentInfo(compartment));
                }
            }
        }
    },

    initVerticalCompartments: function () {
        var x, y, start;
        for (x = 0; x < 9; x++) {
            y = 0;
            while (y < 9) {
                while (y < 9 && !this.boardcolor[y][x]) y++; // skip black
                start = y;
                while (y < 9 && this.boardcolor[y][x]) y++;  // skip white
                if (start != 9) {
                    var compartment = new Str8ts.VCompartment(x, start, y - 1);
                    this.compartments.push(compartment);
                    this.compartmentInfo.push(new Str8ts.CompartmentInfo(compartment));
                }
            }
        }
    },

    initCellsToCompartmentIndexes: function () {
        for (var c = 0; c < this.compartments.length; c++) {
            var compartment = this.compartments[c];
            if (compartment instanceof Str8ts.HCompartment) {
                for (var x = compartment.start; x <= compartment.end; x++) {
                    this.cellToHCompartmentIndex[compartment.row][x] = c;
                }
            }
            else if (compartment instanceof Str8ts.VCompartment) {
                for (var y = compartment.start; y <= compartment.end; y++) {
                    this.cellToVCompartmentIndex[y][compartment.col] = c;
                }
            }
        }
    },

    updateCompartmentInfo: function (markOnly) {
        if (markOnly)
            return;
        for (var c = 0; c < this.compartmentInfo.length; c++) {
            this.compartmentInfo[c].update(false);
        }
    },

    cellToString: function (y, x) {
        var str = '';
        for (var n = 0; n < 9; n++) {
            if (this.board[y][x][n]) {
                str += String.fromCharCode('1'.charCodeAt(0) + n);
            }
        }
        return str;
    },

    redraw: function() {
        var that = this;
        Str8ts.loopXY(function (y, x) {
            if (that.boardcolor[y][x]) {
                that.updateCell(y, x);
            }
        });
    },

    updateCell: function (row, col) {
        var that = this;

        function cellWithDigits(td) {
            td.innerHTML = html;
            td.className = td.className.replace(/ ?(single|empty)/g, "");
            that.solved[row][col] = 0;
        }

        function cellWithSingleDigit(td, digit) {
            td.innerHTML = digit;
            td.className = td.className.replace(/ ?(single|empty)/g, "") + " single";
            that.solved[row][col] = digit;
        }

        function cellWithoutDigits(td) {
            td.innerHTML = html;
            td.className = td.className.replace(/ ?(single|empty)/g, "") + " empty";
            that.solved[row][col] = 0;
        }

        var i, digit, td;
        var numbers = this.board[row][col];
        var html = "";
        var sumDigits = 0;
        var sumRemove = 0;
        var hasSingleMarker = false;
        var lastDigit = "";
        for (i = 1; i <= 9; i++) {
            switch (numbers[i - 1]) {
                case Str8ts.FLAG_DIGIT:
                    digit = i;
                    break;
                case Str8ts.FLAG_DIGIT_TO_REMOVE:
                    digit = "<span class='remove'>" + i + "</span>";
                    sumRemove++;
                    break;
                case Str8ts.FLAG_NEW_SINGLE:
                    digit = "<span class='single'>" + i + "</span>";
                    hasSingleMarker = true;
                    break;
                default:
                    digit = "&nbsp;";
            }
            html += digit;
            if (numbers[i - 1]) {
                sumDigits++;
                lastDigit = i;
            }
            html += (i % 3 == 0) ? "<br>" : " ";
        }
        td = this.domBoard.rows[row].cells[col + 1];
        if (sumDigits > 1) {
            cellWithDigits(td);
        }
        else if (sumDigits == 1) {
            cellWithSingleDigit(td, lastDigit);
        }
        else {
            cellWithoutDigits(td);
        }
        if (hasSingleMarker || (sumRemove && sumDigits - sumRemove == 1)) {
            td.className += " newsingle";
            this.highlightedNewSingle = true;
        }
    }
};


var Str8tsUI = function (board, solver) {
    this.board = board;
    this.domBoard = board.domBoard;
    this.solver = solver;

    this.cursor = {
        row: 0,
        col: 0,
        td: this.domBoard.rows[0].cells[1]
    };
    this.cursor.td.className += " selected";
    this.selectedColor = false;
    this.selectedColorClass = "";
    this.highlighted = false;

    this.showSetti = false;
    this.manualSetti = { row: [], col: [] };
    for (var i = 0; i < 9; i++) {
        this.manualSetti.row[i] = [];
        this.manualSetti.col[i] = [];
    }
};

Str8tsUI.prototype = {

    urlFormHandler: function (ev) {
        ev.preventDefault();
        if (Str8ts.board.initFromQueryString(document.getElementById('str8tsurl').value)) {
            Str8ts.startTime = Date.now();
        }
    },

    toggleChallengeHandler: function(ev) {
        Str8ts.maxStrategySteps = ev.target.checked ? 2 : 99;
        var li = document.getElementById("steps").getElementsByTagName("li");
        for (var i = 0; i < li.length; i++) {
            li[i].style.display = (i < Str8ts.maxStrategySteps) ? "list-item" : "none";
        }
    },

    clearAll: function () {
        this.clearMarkers();
        this.clearNewSingleMarkers();
        this.clearHighlights();
        this.clearColors();
        this.clearSetti();
        this.setCurrentStep(0);
    },

    /**
     * Just clear all set markers from the board (no change of board values)
     * @returns {number} Number of changed cells
     */
    clearMarkers: function () {
        var cells = 0, y, x, changed, num;

        for (y = 0; y < 9; y++) {
            for (x = 0; x < 9; x++) {
                changed = 0;
                for (num = 0; num < 9; num++) {
                    if (this.board.board[y][x][num] > 1) {
                        this.board.board[y][x][num] = 1;
                        changed++
                    }
                }
                if (changed) {
                    this.board.updateCell(y, x);
                    cells++
                }
            }
        }
        return cells;
    },

    clearNewSingleMarkers: function () {
        var that = this;
        Str8ts.loopXY(function (y, x) {
            var c = that.domBoard.rows[y].cells[x + 1];
            if (c.className.match(/newsingle/)) {
                c.className = c.className.replace(/ ?newsingle/g, "");
            }
        });
        this.board.highlightedNewSingle = false;
    },

    clickHandler: function (ev) {
        var td = ev.target;
        if (td.nodeName == "TH") {
            this.manualSettiRowHandler(ev);
            return;
        }
        if (td.nodeName == "SPAN") {
            td = td.parentNode;
        }
        if (td.nodeName != "TD") {
            return;
        }
        var col = td.cellIndex - 1;
        var row = td.parentNode.rowIndex - 1;
        var locked = this.board.lockedcell[row][col];
        if (ev.altKey) {
            this.setCursorPosition(row, col, td);
            ev.preventDefault();
        }
        else if (this.board.boardcolor[row][col]) {
            if (this.selectedColor) {
                var cell = this.domBoard.rows[row].cells[col + 1];
                if (cell.className.match(/mark-/)) {
                    cell.className = cell.className.replace(/ ?mark-[a-z]+/, "");
                } else {
                    cell.className += " " + this.selectedColorClass;
                }
            }
            else if (!locked) {
                Str8ts.history.push(this.board.getClone());
                var r = td.getBoundingClientRect();
                var cellWidth = r.width / 3;
                var cellHeight = r.height / 3;
                var cx = ev.clientX - r.left;
                var cy = ev.clientY - r.top;
                var x = (cx > 2 * cellWidth - 4) ? 2 : ((cx < cellWidth) ? 0 : 1);
                var y = (cy > 2 * cellHeight - 4) ? 2 : ((cy < cellHeight) ? 0 : 1);
                var num = x + y * 3;
                if (ev.ctrlKey) {
                    this.board.board[row][col] = [0,0,0,0,0,0,0,0,0];
                    this.board.board[row][col][num] = Str8ts.FLAG_DIGIT;
                } else {
                    this.board.board[row][col][num] = this.board.board[row][col][num] ? Str8ts.FLAG_NO_DIGIT : Str8ts.FLAG_DIGIT;
                }
                this.board.updateCell(row, col);
                this.board.compartmentInfo[this.board.cellToHCompartmentIndex[row][col]].update();
                this.board.compartmentInfo[this.board.cellToVCompartmentIndex[row][col]].update();
            }
            ev.preventDefault();
        }
    },

    keyPressHandler: function (ev) {
        if (ev.altKey || ev.ctrlKey) {
            return;
        }
        if (ev.charCode >= 49 && ev.charCode <= 57) {
            if (!this.board.lockedcell[this.cursor.row][this.cursor.col]) {
                this.numberPressed(ev.charCode - 48, false);
            }
            ev.preventDefault();
        }
        else if (ev.charCode == 115 || ev.charCode == 110 || ev.charCode == 108) { // 115=s, 110=n, 108=l
            var maxStep = (ev.charCode == 108) ? 2 : Str8ts.maxStrategySteps;
            this.executeStep(ev.charCode != 115, maxStep);
            ev.preventDefault();
        }
        else if (ev.charCode == 104) { // h == hint
            this.executeStep(true, 99);
            this.clearMarkers();
            ev.preventDefault();
        }
        else if (ev.charCode == 117) { // 117 == u
            if (Str8ts.history.length) {
                var oldBoard = Str8ts.history.pop();
                Str8ts.setNewBoard(oldBoard);
                this.board.redraw();
                this.clearAll();
                ev.preventDefault();
            }
        }
    },

    numberPressed: function (num, ctrl) {
        var row = this.cursor.row;
        var col = this.cursor.col;
        if (!this.board.boardcolor[row][col]) {	// black
            return;
        }
        Str8ts.history.push(this.board.getClone());
        if (ctrl) {
            this.board.board[row][col] = [0, 0, 0, 0, 0, 0, 0, 0, 0];
            this.board.board[row][col][num - 1] = 1;
        } else {
            this.board.board[row][col][num - 1] = this.board.board[row][col][num - 1] ? Str8ts.FLAG_NO_DIGIT : Str8ts.FLAG_DIGIT;
        }
        this.board.updateCell(row, col);
        this.board.compartmentInfo[this.board.cellToHCompartmentIndex[row][col]].update();
        this.board.compartmentInfo[this.board.cellToVCompartmentIndex[row][col]].update();
    },

    keyDownHandler: function (ev) {
        var that = this;

        function handleHighlighting(ev) {
            var hlElem, num, setti;

            hlElem = document.getElementById("highlight");
            if (!that.highlighted) {
                if (!ev.ctrlKey || !that.showSetti) {
                    that.doHighlight(hlElem.firstChild);
                } else {
                    num = that.showSetti.firstChild.nodeValue;
                    for (setti = hlElem.firstChild; setti.firstChild.nodeValue != num; setti = setti.nextSibling) {}
                    that.doHighlight(setti);
                }
            } else {
                if (ev.keyCode == 37 && that.highlighted != hlElem.firstChild) { // left
                    that.doHighlight(that.highlighted.previousSibling);
                }
                if (ev.keyCode == 39 && that.highlighted != hlElem.lastChild) { // right
                    that.doHighlight(that.highlighted.nextSibling);
                }
            }
        }

        function handleSetti(ev) {
            var settiElem = document.getElementById("setti");
            if (!that.showSetti) {
                that.doSetti(settiElem.firstChild);
            } else {
                if (ev.keyCode == 37 && that.showSetti != settiElem.firstChild) { // left
                    that.doSetti(that.showSetti.previousSibling);
                }
                if (ev.keyCode == 39 && that.showSetti != settiElem.lastChild) { // right
                    that.doSetti(that.showSetti.nextSibling);
                }
            }
        }

        function handleCursor(ev) {
            if ((ev.shiftKey || ev.ctrlKey) && (ev.keyCode == 37 || ev.keyCode == 39)) {
                handleSetti(ev);
            }
            if ((ev.altKey || ev.ctrlKey) && (ev.keyCode == 37 || ev.keyCode == 39)) {
                handleHighlighting(ev); // must be after handleSetti()
            }
            if (!ev.altKey && !ev.shiftKey && !ev.ctrlKey) {
                var key = ev.keyCode == 9 ? 39 : ev.keyCode; // TAB -> right arrow
                that.moveCursor(key);
            }
            ev.preventDefault();
        }

        function handleNumbers(ev) {
            if (ev.ctrlKey) {
                if (!that.board.lockedcell[that.cursor.row][that.cursor.col]) {
                    that.numberPressed(ev.keyCode - 48, true);
                }
            } else if (ev.altKey) {
                that.doHighlight(document.getElementById("highlight").childNodes[ev.keyCode - 49]);
            }
        }

        if ((ev.keyCode >= 37 && ev.keyCode <= 40) || ev.keyCode == 9) {    // Cursor and TAB
            handleCursor(ev);
        }
        else if (ev.keyCode == 27) { // ESC
            this.clearAll();
            ev.preventDefault();
        }
        else if ((ev.altKey || ev.ctrlKey) && ev.keyCode >= 49 && ev.keyCode <= 57) {
            handleNumbers(ev);
            ev.preventDefault();
        }
    },

    setCursorPosition: function (row, col, td) {
        td.className += " selected";
        this.cursor.td.className = this.cursor.td.className.replace(/ ?selected/, "");
        this.cursor.td = td;
        this.cursor.col = col;
        this.cursor.row = row;
    },

    moveCursor: function (code) {
        var newColumn = this.cursor.col;
        var newRow = this.cursor.row;
        switch (code) {
            case 37:    // cursor left
                if (this.cursor.col > 0) newColumn--;
                break;
            case 39:    // cursor right
                if (this.cursor.col < 8) newColumn++;
                break;
            case 38:    // cursor up
                if (this.cursor.row > 0) newRow--;
                break;
            case 40:    // cursor down
                if (this.cursor.row < 8) newRow++;
                break;
        }
        var newTd = this.domBoard.rows[newRow].cells[newColumn + 1];
        if (this.cursor.td != newTd) {
            this.setCursorPosition(newRow, newColumn, newTd);
        }
    },

    highlightHandler: function (ev) {
        if (ev.target.nodeName == "SPAN") {
            this.doHighlight(ev.target);
        }
    },

    doHighlight: function (numberElement) {
        var oldHighlight = false;
        if (this.highlighted) {
            oldHighlight = this.clearHighlights();
        }
        if (oldHighlight != numberElement) {
            this.setHighlights(numberElement);
        }
    },

    // clears highlights and colors
    clearHighlights: function () {
        var oldHighlight = this.highlighted;
        this.highlighted.className = "";
        this.highlighted = false;
        var that = this;
        Str8ts.loopXY(function (y, x) {
            var c = that.domBoard.rows[y].cells[x+1];
            if (c.className.match(/highlight|selected/)) {
                c.className = c.className.replace(/ ?(highlight|selected)/g, "");
            }
        });
        return oldHighlight;
    },

    setHighlights: function (numberElement) {
        var digit = numberElement.firstChild.nodeValue;
        this.highlighted = numberElement;
        this.highlighted.className = "highlight";
        var that = this;
        Str8ts.loopXY(function (y, x) {
            if (that.board.board[y][x][digit - 1]) {
                that.domBoard.rows[y].cells[x + 1].className += " highlight";
            }
        });
    },

    colorsHandler: function (ev) {
        if (ev.target.nodeName != "SPAN") {
            return;
        }
        var elem = ev.target;
        var className = "mark-" + elem.firstChild.nodeValue;
        if (className == "mark-white") {
            this.domBoard.className = (this.domBoard.className == "") ? "white" : ""
        } else {
            var setNewColor = (elem != this.selectedColor);
            if (this.selectedColor) {
                this.selectedColor.className = "";
                this.selectedColor = false;
                this.selectedColorClass = "";
            }
            if (setNewColor) {
                this.selectedColor = elem;
                this.selectedColorClass = "mark-" + elem.firstChild.nodeValue;
                elem.className = this.selectedColorClass;
            }
        }
    },

    clearColors: function() {
        var that = this;
        Str8ts.loopXY(function (y, x) {
            var c = that.domBoard.rows[y].cells[x+1];
            if (c.className.match(/mark-/)) {
                c.className = c.className.replace(/ ?mark-[a-z]+/g, "");
            }
        });
        this.selectedColor.className = "";
        this.selectedColor = false;
        this.selectedColorClass = "";
    },

    manualSettiColumnHandler: function (ev) {
        var th = ev.target;
        this.updateManualSetti("col", th.cellIndex - 1, th);
    },

    manualSettiRowHandler: function (ev) {
        var th = ev.target;
        this.updateManualSetti("row", th.parentNode.rowIndex - 1, th);
    },

    updateManualSetti: function (type, num, th) {
        if (!this.showSetti) {
            return;
        }
        var candidate = parseInt(this.showSetti.firstChild.nodeValue, 10) - 1;
        if (th.className == "setti-yes" && this.manualSetti[type][num][candidate]) {
            th.className = "setti-maybe";
            this.manualSetti[type][num][candidate] = false;
        }
        else if (th.className == "setti-maybe" && !this.manualSetti[type][num][candidate]) {
            th.className = "setti-yes";
            this.manualSetti[type][num][candidate] = true;
        }
    },

    settiHandler: function (ev) {
        if (ev.target.nodeName == "SPAN") {
            this.doSetti(ev.target);
        }
    },

    doSetti: function (settiElement) {
        var oldSetti = false;
        if (this.showSetti) {
            oldSetti = this.clearSetti();
        }
        if (oldSetti != settiElement) {
            this.setSetti(settiElement);
        }
    },

    clearSetti: function () {
        var oldSetti = this.showSetti;
        var cols = document.getElementById("bcolumns");
        for (var i = 0; i < 9; i++) {
            this.domBoard.rows[i].cells[0].className = "";
            cols.cells[i + 1].className = "";
        }
        this.showSetti.className = "";
        this.showSetti = false;
        return oldSetti;
    },

    setSetti: function (settiElement) {
        var num = parseInt(settiElement.firstChild.nodeValue, 10) - 1;
        this.showSetti = settiElement;
        this.showSetti.className = "highlight";
        var cols = document.getElementById("bcolumns");
        for (var i = 0; i < 9; i++) {
            this.checkSetti(num, i, true, this.domBoard.rows[i].cells[0]);
            this.checkSetti(num, i, false, cols.cells[i + 1]);
        }
    },

    checkSetti: function (num, cur, isRow, th) {
        var check = isRow ? "row" : "col";
        var comps = [];
        for (var i = 0; i < this.board.compartmentInfo.length; i++) {
            var compartment = this.board.compartments[i];
            var compartmentInfo = this.board.compartmentInfo[i];
            if ((!isRow && (compartment instanceof Str8ts.HCompartment)) || (isRow && (compartment instanceof Str8ts.VCompartment))) {
                continue;
            }
            if (compartment[check] != cur) {
                continue;
            }
            comps.push(compartmentInfo);
            if (num >= compartmentInfo.sureMin && num <= compartmentInfo.sureMax) {
                th.className = "setti-yes";
                return;
            }
        }
        var exists = false;
        for (i = 0; i < comps.length; i++) {
            var ci = comps[i];
            for (var j = ci.compartment.start; j <= ci.compartment.end; j++) {
                if (ci.compartment.isSolved(j) == num + 1) {
                    th.className = "setti-yes";
                    return;
                }
                if (ci.digitCount[num]) {
                    exists = true;
                }
            }
        }
        if (!exists) {
            th.className = "setti-no";
            return;
        }
        th.className = this.manualSetti[check][cur][num] ? "setti-yes" : "setti-maybe";
    },

    executeStep: function (markOnly, maxStep) {
        if (this.highlighted) {
            this.clearHighlights();
        }
        if (this.board.highlightedNewSingle) {
            this.clearNewSingleMarkers();
        }

        Str8ts.history.push(this.board.getClone());

        if (this.solver.executeMarkers()) {
            this.setCurrentStep(1);
            this.board.updateCompartmentInfo(false);
            return;
        }

        var steps = [
            this.solver.showPossible,
            this.solver.compartmentRange,
            this.solver.checkSingle,
            this.solver.dividedCompartment,
            this.solver.checkHighLow,
            this.solver.intraCompartment,
            this.solver.nakedPair,
            this.solver.hiddenPair,
            this.solver.hiddenTriple
        ];

        if (!maxStep) {
            maxStep = Str8ts.maxStrategySteps;
        }
        for (var s = 0; s < steps.length && s + 1 < maxStep; s++) {
            if (steps[s].call(this.solver, markOnly)) {
                this.setCurrentStep(s + 2);
                this.board.updateCompartmentInfo(markOnly);
                markOnly && Str8ts.history.pop();
                return;
            }
        }
        Str8ts.history.pop();
        this.setCurrentStep(0);
    },

    setCurrentStep: function (num) {
        var i, li;
        li = document.getElementById("steps").getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            li[i].className = (i == num - 1) ? "current" : "";
        }
    }
};


var Solver = function (board) {
    this.board = board;
};

Solver.prototype = {

    /**
     * Remove all markers from the board, and set board data accordingly
     * @returns {boolean} Whether cells have changed or not
     */
    executeMarkers: function () {
        var cellsChanged = false, that = this;
        Str8ts.loopXY(function (y, x) {
            var changed = false;
            var board = that.board.board;
            for (var n = 0; n < 9; n++) {
                if (board[y][x][n] == Str8ts.FLAG_DIGIT_TO_REMOVE) {
                    board[y][x][n] = 0;
                    changed = true;
                }
                else if (board[y][x][n] == Str8ts.FLAG_NEW_SINGLE) {
                    board[y][x] = [0, 0, 0, 0, 0, 0, 0, 0, 0];
                    board[y][x][n] = 1;
                    changed = true;
                    break;
                }
            }
            if (changed) {
                that.board.updateCell(y, x);
                cellsChanged = true;
            }
        });
        return cellsChanged;
    },

    showPossible: function (markOnly) {
        var cellsChanged = false, that = this, remove;

        remove = markOnly ? Str8ts.FLAG_DIGIT_TO_REMOVE : Str8ts.FLAG_NO_DIGIT;
        Str8ts.loopXY(function (y, x) {
            if (!that.board.solved[y][x]) {
                return;
            }
            var digit = that.board.solved[y][x] - 1;
            for (var xy = 0; xy < 9; xy++) {
                if (xy != x && that.board.board[y][xy][digit]) {
                    that.board.board[y][xy][digit] = remove;
                    that.board.updateCell(y, xy);
                    cellsChanged = true;
                }
                if (xy != y && that.board.board[xy][x][digit]) {
                    that.board.board[xy][x][digit] = remove;
                    that.board.updateCell(xy, x);
                    cellsChanged = true;
                }
            }
        });
        return cellsChanged;
    },

    compartmentRange: function (markOnly) {
        var cellsChanged = false, i;

        function removeDigitsOutsideOfMinMax(compartment, compartmentInfo, markOnly) {
            var cell, n, change;

            for (cell = compartment.start; cell <= compartment.end; cell++) {
                change = 0;
                for (n = 0; n < compartmentInfo.min; n++) {
                    change += compartment.delNum(cell, n, markOnly);
                }
                for (n = compartmentInfo.max + 1; n < 9; n++) {
                    change += compartment.delNum(cell, n, markOnly);
                }
                if (change) {
                    compartment.updateCell(cell);
                    cellsChanged = true;
                }
            }
        }

        for (i = 0; i < this.board.compartments.length; i++) {
            removeDigitsOutsideOfMinMax(this.board.compartments[i], this.board.compartmentInfo[i], markOnly);
        }
        return cellsChanged;
    },

    dividedCompartment: function (markOnly) {
        var cellsChanged = false, i, digitsToDelete;

        function findStrandedDigits(compartment, compartmentInfo) {
            var digit, startOfRange, deleteDigits;

            digit = compartmentInfo.min;
            startOfRange = compartmentInfo.min;
            deleteDigits = [];
            while (digit <= compartmentInfo.max + 1) {
                if (!compartmentInfo.digitCount[digit] || digit > compartmentInfo.max) {
                    if (digit - startOfRange < compartment.len) {
                        for (; startOfRange < digit; startOfRange++)
                            deleteDigits.push(startOfRange)
                    }
                    if (digit > compartmentInfo.max) {
                        break;
                    }
                    while (!compartmentInfo.digitCount[digit] && digit <= compartmentInfo.max + 1) {
                        digit++;
                    }
                    startOfRange = digit;
                } else {
                    digit++;
                }
            }
            return deleteDigits;
        }

        function removeStrandedDigits(compartment, digitsToDelete, markOnly) {
            var cell, i, change;
            for (cell = compartment.start; cell <= compartment.end; cell++) {
                change = 0;
                for (i = 0; i < digitsToDelete.length; i++) {
                    change += compartment.delNum(cell, digitsToDelete[i], markOnly);
                }
                if (change) {
                    compartment.updateCell(cell);
                    cellsChanged = true;
                }
            }
        }

        for (i = 0; i < this.board.compartments.length; i++) {
            digitsToDelete = findStrandedDigits(this.board.compartments[i], this.board.compartmentInfo[i]);
            if (digitsToDelete) {
                removeStrandedDigits(this.board.compartments[i], digitsToDelete, markOnly);
            }
        }
        return cellsChanged;
    },

    checkHighLow: function (markOnly) {
        var cellsChanged = false, i, ci;

        function removeHighLowRange(compartment, compartmentInfo, markOnly) {
            var cell, change, n;
            for (cell = 0; cell < 9; cell++) {
                if (cell >= compartment.start && cell <= compartment.end) {
                    continue;
                }
                if (!compartment.isWhite(cell) || compartment.isLocked(cell)) {
                    continue;
                }
                change = 0;
                for (n = compartmentInfo.sureMin; n <= compartmentInfo.sureMax; n++)
                    change += compartment.delNum(cell, n, markOnly);
                if (change) {
                    compartment.updateCell(cell);
                    cellsChanged = true;
                }
            }
        }

        for (i = 0; i < this.board.compartments.length; i++) {
            ci = this.board.compartmentInfo[i];
            if (ci.sureMin <= ci.sureMax) {
                removeHighLowRange(this.board.compartments[i], ci, markOnly);
            }
        }
        return cellsChanged;
    },

    checkSingle: function (markOnly) {
        var cellsChanged = false, i;

        function findSingle(compartment, compartmentInfo, markOnly) {
            var i, n, pos;
            for (n = compartmentInfo.sureMin; n <= compartmentInfo.sureMax; n++) {
                if (compartmentInfo.digitCount[n] == 1) {
                    pos = compartmentInfo.digitPosition[n][0];
                    if (!compartment.isSolved(pos)) {
                        if (markOnly) {
                            compartment.setNum(pos, n, Str8ts.FLAG_NEW_SINGLE);
                        }
                        else {
                            for (i = 0; i < 9; i++) {
                                compartment.setNum(pos, i, (n == i) ? Str8ts.FLAG_DIGIT : Str8ts.FLAG_NO_DIGIT);
                            }
                        }
                        compartment.updateCell(pos);
                        cellsChanged = true;
                    }
                }
            }
        }

        for (i = 0; i < this.board.compartments.length; i++) {
            findSingle(this.board.compartments[i], this.board.compartmentInfo[i], markOnly);
        }
        return cellsChanged;
    },

    intraCompartment: function (markOnly) {
        var cellsChanged = false, i, c, n, j, compartment, otherDigits, change, found, digits;

        for (i = 0; i < this.board.compartments.length; i++) {
            compartment = this.board.compartments[i];
            if (compartment.len == 1) {
                continue;
            }
            for (c = compartment.start; c <= compartment.end; c++) {
                change = false;
                digits = compartment.get(c);
                for (n = 0; n < 9; n++) {
                    if (!digits[n]) {
                        continue;
                    }
                    found = false;
                    for (j = compartment.start; j <= compartment.end; j++) {
                        if (j == c) {
                            continue;
                        }
                        otherDigits = compartment.get(j);
                        if ((n > 0 && otherDigits[n-1] == Str8ts.FLAG_DIGIT) || (n < 8 && otherDigits[n+1] == Str8ts.FLAG_DIGIT)) {
                            found = true;
                            break;
                        }
                    }
                    if (!found && !compartment.isLocked(c)) {
                        change = true;
                        compartment.delNum(c, n, markOnly);
                    }
                }
                if (change) {
                    compartment.updateCell(c);
                    cellsChanged = true;
                }
            }
        }
        return cellsChanged;
    },

    nakedPair: function (markOnly) {
        var cellsChanged = false, board, that = this;

        function boardToStrings () {
            var board = new Array(9);
            for (var y = 0; y < 9; y++) {
                board[y] = new Array(9);
                for (var x = 0; x < 9; x++) {
                    if (that.board.boardcolor[y][x]) {
                        board[y][x] = that.board.cellToString(y, x);
                    }
                }
            }
            return board;
        }

        function removeDigitsByString (y, x, digits, markOnly) {
            var remove = markOnly ? Str8ts.FLAG_DIGIT_TO_REMOVE : Str8ts.FLAG_NO_DIGIT;
            var change = 0;
            for (var c = 0; c < digits.length; c++) {
                var n = digits.charCodeAt(c) - '1'.charCodeAt(0);
                if (that.board.board[y][x][n]) {
                    change++;
                    that.board.board[y][x][n] = remove;
                }
            }
            if (change) {
                that.board.updateCell(y, x);
                return true;
            }
            return false;
        }

        board = boardToStrings();
        Str8ts.loopXY(function (y, x) {     // row
            if (!that.board.boardcolor[y][x] || board[y][x].length != 2) {
                return;
            }
            for (var n = x + 1; n < 9; n++) {
                if (board[y][x] === board[y][n]) {
                    for (var n2 = 0; n2 < 9; n2++) {
                        if (that.board.boardcolor[y][n2] && !that.board.lockedcell[y][n2] && n2 != x && n2 != n) {
                            if (removeDigitsByString(y, n2, board[y][x], markOnly)) {
                                cellsChanged = true;
                            }
                        }
                    }
                }
            }
        });
        Str8ts.loopXY(function (x, y) {     // column
            if (!that.board.boardcolor[y][x] || board[y][x].length != 2) {
                return;
            }
            for (var n = y + 1; n < 9; n++) {   // column
                if (board[y][x] === board[n][x]) {
                    for (var n2 = 0; n2 < 9; n2++) {
                        if (that.board.boardcolor[n2][x] && !that.board.lockedcell[n2][x] && n2 != y && n2 != n) {
                            if (removeDigitsByString(n2, x, board[y][x], markOnly)) {
                                cellsChanged = true;
                            }
                        }
                    }
                }
            }
        });
        return cellsChanged;
    },

    hiddenPair: function (markOnly) {
        var cellsChanged = false, c, n1, n2, n, info, change;

        for (c = 0; c < this.board.compartmentInfo.length; c++) {
            info = this.board.compartmentInfo[c];
            for (n1 = info.sureMin; n1 <= info.sureMax; n1++) {
                if (info.digitCount[n1] != 2) {
                    continue;
                }
                for (n2 = n1 + 1; n2 <= info.sureMax; n2++) {
                    if (info.digitCount[n2] != 2) {
                        continue;
                    }
                    if (info.digitPosition[n1][0] == info.digitPosition[n2][0]
                        && info.digitPosition[n1][1] == info.digitPosition[n2][1])
                    {
                        change = 0;
                        for (n = 0; n < 9; n++) {
                            if (n == n1 || n == n2)
                                continue;
                            change += info.compartment.delNum(info.digitPosition[n1][0], n, markOnly);
                            change += info.compartment.delNum(info.digitPosition[n1][1], n, markOnly);
                        }
                        if (change) {
                            info.compartment.updateCell(info.digitPosition[n1][0]);
                            info.compartment.updateCell(info.digitPosition[n1][1]);
                            cellsChanged = true;
                        }
                    }
                }
            }
        }
        return cellsChanged;
    },

    hiddenTriple: function (markOnly) {
        var cellsChanged = false, c, n1, n2, n3, n, info, change;

        for (c = 0; c < this.board.compartmentInfo.length; c++) {
            info = this.board.compartmentInfo[c];
            for (n1 = info.sureMin; n1 <= info.sureMax; n1++) {
                if (info.digitCount[n1] != 3) {
                    continue;
                }
                for (n2 = n1 + 1; n2 <= info.sureMax; n2++) {
                    if (info.digitCount[n2] != 3) {
                        continue;
                    }
                    for (n3 = n2 + 1; n3 <= info.sureMax; n3++) {
                        if (info.digitCount[n3] != 3) {
                            continue;
                        }
                        if (info.digitPosition[n1][0] == info.digitPosition[n2][0]
                            && info.digitPosition[n1][1] == info.digitPosition[n2][1]
                            && info.digitPosition[n1][2] == info.digitPosition[n2][2]
                            && info.digitPosition[n1][0] == info.digitPosition[n3][0]
                            && info.digitPosition[n1][1] == info.digitPosition[n3][1]
                            && info.digitPosition[n1][2] == info.digitPosition[n3][2])
                        {
                            change = 0;
                            for (n = 0; n < 9; n++) {
                                if (n == n1 || n == n2 || n == n3)
                                    continue;
                                change += info.compartment.delNum(info.digitPosition[n1][0], n, markOnly);
                                change += info.compartment.delNum(info.digitPosition[n1][1], n, markOnly);
                                change += info.compartment.delNum(info.digitPosition[n1][2], n, markOnly);
                            }
                            if (change) {
                                info.compartment.updateCell(info.digitPosition[n1][0]);
                                info.compartment.updateCell(info.digitPosition[n1][1]);
                                info.compartment.updateCell(info.digitPosition[n1][2]);
                                cellsChanged = true;
                            }
                        }
                    }
                }
            }
        }
        return cellsChanged;
    }
};
